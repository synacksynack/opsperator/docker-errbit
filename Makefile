SKIP_SQUASH?=1
IMAGE = opsperator/errbit
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: demodb
demodb:
	@@docker rm -f testmongodb || true
	@@docker run --name testmongodb \
	    -e DEBUG=yay \
	    -e MONGODB_ADMIN_PASSWORD=mgadminpassword \
	    -e MONGODB_DATABASE=mgdb \
	    -e MONGODB_KEYFILE_VALUE=kfjghkfjh \
	    -e MONGODB_PASSWORD=mgpassword \
	    -e MONGODB_REPLICA_NAME=replset \
	    -e MONGODB_SERVICE_NAME=localhost \
	    -e MONGODB_USER=mguser \
	    -p 27017:27017 \
	    -d registry.gitlab.com/synacksynack/opsperator/docker-mongodb:master

.PHONY: demodbinit
demodbinit:
	@@cpt=0; \
	while true; \
	do \
	    if docker exec testmongodb /bin/sh -c /usr/bin/is-master.sh; then \
		break; \
	    elif test $$cpt -ge 10; then \
		exit 1; \
	    fi; \
	    echo Waiting for MongoDB to start ...; \
	    cpt=`expr $$cpt + 1`; \
	    sleep 10; \
	done
	@@docker rm -f testdbinit || true
	@@mgip=`docker inspect testmongodb | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	echo "Working with mongodb=$$mgip"; \
	docker run --name testdbinit \
	    --entrypoint /bin/sh \
	    -e DEBUG=yay \
	    -e EMAIL_DELIVERY_METHOD=:smtp \
	    -e "MONGODB_URL=mongodb://mguser:mgpassword@$$mgip/mgdb" \
	    -e ERRBIT_ADMIN_EMAIL=errbitadmin@demo.local \
	    -e ERRBIT_ADMIN_PASSWORD=secretadm1npw \
	    -e ERRBIT_ADMIN_USER=adminci \
	    -e ERRBIT_EMAIL_FROM=noreply@demo.local \
	    -e ERRBIT_HOST=errbit.demo.local \
	    -e ERRBIT_PROTOCOL=https \
	    -e GITHUB_AUTHENTICATION=false \
	    -e GOOGLE_AUTHENTICATION=false \
	    -e PORT=8080 \
	    -e RACK_ENV=production \
	    -e RAILS_ENV=production \
	    -e SECRET_KEY_BASE=thisismyerrbitsecretkeyforci \
	    -it $(IMAGE) \
	    -c 'set -x && bundle exec rake errbit:bootstrap'

.PHONY: demo
demo:
	@@docker rm -f testerrbit || true
	@@mgip=`docker inspect testmongodb | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	echo "Working with mongodb=$$mgip"; \
	docker run --name testerrbit \
	    -e DEBUG=yay \
	    -e "MONGODB_URL=mongodb://mguser:mgpassword@$$mgip/mgdb" \
	    -e ERRBIT_ADMIN_EMAIL=errbitadmin@demo.local \
	    -e ERRBIT_ADMIN_PASSWORD=secretadm1npw \
	    -e ERRBIT_ADMIN_USER=adminci \
	    -e ERRBIT_EMAIL_FROM=noreply@demo.local \
	    -e ERRBIT_HOST=errbit.demo.local \
	    -e ERRBIT_PROTOCOL=https \
	    -e PORT=8080 \
	    -e RACK_ENV=production \
	    -e RAILS_ENV=production \
	    -d $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "ERRBIT_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "ERRBIT_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: ocpurge
ocpurge:
	@@oc process -f deploy/openshift/build-with-secret.yaml \
	    -p GIT_DEPLOYMENT_TOKEN=toto | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
