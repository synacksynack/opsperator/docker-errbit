FROM docker.io/ruby:2.7.6-alpine

# Errbit image for OpenShift Origin

ENV BUNDLER_VERSION=2.3.15 \
    ERRBIT_VERSION=0.9.1 \
    RUBYGEMS_VERSION=3.3.15

LABEL io.k8s.description="Errbit $ERRBIT_VERSION." \
      io.k8s.display-name="Errbit $ERRBIT_VERSION" \
      io.openshift.tags="elasticsearch,curator" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-errbit" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$ERRBIT_VERSION"

WORKDIR /app

RUN set -x \
    && echo "gem: --no-document" >>/etc/gemrc \
    && bundle config --global frozen 1 \
    && bundle config --global clean true \
    && bundle config --global disable_shared_gems false \
    && gem update --system $RUBYGEMS_VERSION \
    && gem install bundler --version $BUNDLER_VERSION \
    && apk add --no-cache curl less libxml2-dev libxslt-dev nodejs tzdata

COPY Gemfile Gemfile.lock /app/

RUN set -x \
    && apk add --no-cache --virtual build-dependencies build-base \
    && bundle config build.nokogiri --use-system-libraries \
    && bundle config set without 'test development no_docker' \
    && bundle config --global clean false \
    && bundle install -j "$(getconf _NPROCESSORS_ONLN)" --retry 5 \
    && bundle clean --force \
    && apk del build-dependencies

COPY . /app

RUN set -x \
    && RAILS_ENV=production bundle exec rake assets:precompile \
    && rm -rf /app/tmp/* \
    && chown 1001:0 /app/tmp \
    && chmod g=u /app/tmp

CMD ["bundle","exec","puma","-C","config/puma.default.rb"]
EXPOSE 8080
USER 1001
